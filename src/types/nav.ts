export type NavItem = {
    label: string;
    dataRef: string;
    menuItems?: NavItem[];
    url?: string;
    icon?: string;
    disabled?: boolean;
    descriptor?: string;
    i18nKey?: string;
};