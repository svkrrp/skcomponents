import { Component, h, Prop } from '@stencil/core';

@Component({
  tag: 'sk-tab-with-leading-and-trailing-icon',
  styleUrl: 'sk-tab-with-leading-and-trailing-icon.css',
  shadow: true,
})
export class SkTabWithLeadingAndTrailingIcon {
  @Prop() shouldShowLeadingIcon: boolean;
  @Prop() leadingIcon: string;
  @Prop() shouldShowTrailingIcon: boolean;
  @Prop() trailingIcon: string;
  @Prop() text: string;
  @Prop() showNotification: boolean;
  @Prop() notification: string;

  render() {
    return (
      <div class="flex items-center w-full p-3 font-sans transition duration-75 rounded-lg group dark:text-gray-600 dark:bg-gray-200">
        {this.shouldShowLeadingIcon && <svg class="flex-shrink-0 w-5 h-5 transition duration-75" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 18 21">
          <path d={this.leadingIcon}/>
        </svg>}
        <span class="flex-1 ml-3 text-left whitespace-nowrap">{this.text}</span>
        {this.showNotification && <div class="text-sm bg-gray-700 text-white px-2 rounded-xl">{this.notification}</div>}
        {this.shouldShowTrailingIcon && <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="w-6 h-6">
          <path d={this.trailingIcon} />
        </svg>}
      </div>
    );
  }

}
