import { newSpecPage } from '@stencil/core/testing';
import { SkTabWithLeadingAndTrailingIcon } from '../sk-tab-with-leading-and-trailing-icon';

describe('sk-tab-with-leading-and-trailing-icon', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [SkTabWithLeadingAndTrailingIcon],
      html: `<sk-tab-with-leading-and-trailing-icon></sk-tab-with-leading-and-trailing-icon>`,
    });
    expect(page.root).toEqualHtml(`
      <sk-tab-with-leading-and-trailing-icon>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </sk-tab-with-leading-and-trailing-icon>
    `);
  });
});
