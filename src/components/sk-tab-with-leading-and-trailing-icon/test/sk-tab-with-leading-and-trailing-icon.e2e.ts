import { newE2EPage } from '@stencil/core/testing';

describe('sk-tab-with-leading-and-trailing-icon', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<sk-tab-with-leading-and-trailing-icon></sk-tab-with-leading-and-trailing-icon>');

    const element = await page.find('sk-tab-with-leading-and-trailing-icon');
    expect(element).toHaveClass('hydrated');
  });
});
