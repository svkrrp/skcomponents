import { Component, h, Prop } from '@stencil/core';
import parseJson from '../../utils/utils';

@Component({
  tag: 'sk-table',
  styleUrl: 'sk-table.css',
  shadow: true,
})
export class SkTable {

  @Prop() headers: string;
  @Prop() rows: string;

  render() {
    if (!this.headers || !this.rows) {
      console.warn("Something is not correct");
      return null;
    }

    const headersList = parseJson(this.headers, "", []);
    const contentList = parseJson(this.rows, "", [[]]);

    return (
      <div class="relative overflow-x-auto font-sans rounded-xl">
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
          <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
              {headersList.map((header: string) => <th scope="col" class="px-6 py-3">
                {header}
              </th>)}
            </tr>
          </thead>
          <tbody>
            {contentList.map((row: string[]) => <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
              {row.map(column => <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                {column}
              </th>)}
            </tr>)}
          </tbody>
        </table>
      </div>
    );
  }

}
