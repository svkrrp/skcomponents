import { newSpecPage } from '@stencil/core/testing';
import { SkTable } from '../sk-table';

describe('sk-table', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [SkTable],
      html: `<sk-table></sk-table>`,
    });
    expect(page.root).toEqualHtml(`
      <sk-table>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </sk-table>
    `);
  });
});
