import { newE2EPage } from '@stencil/core/testing';

describe('sk-table', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<sk-table></sk-table>');

    const element = await page.find('sk-table');
    expect(element).toHaveClass('hydrated');
  });
});
