export function format(first: string, middle: string, last: string): string {
  return (first || '') + (middle ? ` ${middle}` : '') + (last ? ` ${last}` : '');
}

export default function parseJson(json: any, errorMessage = '', defaultReturn: any = []) {
  if (typeof json === 'string') {
    try {
      return JSON.parse(json);
    } catch (err) {
      console.error(`Error parsing JSON! ${errorMessage || 'Please provide valid JSON object'}`);

      return defaultReturn;
    }
  }
  return json || defaultReturn;
}
